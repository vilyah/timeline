Description
===

This script takes a "product backlog"-style list of features and time
estimates and produces a timeline for them, in HTML form.

The input is a csv file where each row has a feature name in column A and an
estimate in whole days in column B.

You can optionally specify the number of people working on the project, the
project name and the start date as command line arguments. If you don't
specify these, they default to something (hopefully) sensible.

The output is HTML, printed to stdout. You can redirect this to a file on your
local disk and open the file in a web browser for easy viewing.

The timeline assigns tasks to people in the order they appear in the input
file. If you've already ordered the items in the file from highest to lowest
priority (for example), then the timeline will show the tasks being worked on
in priority order.


Usage examples
===

View the built-in help:

    python timeline.py --help

Generate a timeline for a project called "World Domination", with 3 people on
the team and a start date of April 26th, 2016:

    python timeline.py --project-name "World Domination" --people 3 --start-date 2015-04-26 WorldDomination.csv > WorldDomination.htl


Limitations
===

* The timeline doesn't take vacations, public holidays or part time work weeks
  into account (yet). Everyone on the project is assumed to be available 5 days
  a week - Monday through to Friday - every week.
* Features are assumed to be independent of each other. There's no way to
  specify that one feature can only start after another finishes.
* Features can only be worked on by one person. There's no way to split a
  feature between two or more people, other than by manually breaking it up
  into multiple features.
