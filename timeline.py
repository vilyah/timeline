#!/bin/env python
# (c) 2015 The Foundry

import sys, datetime, optparse, csv

DESCRIPTION = """\
This script takes a list of features and time estimates and produces a
timeline for them, in HTML form. The input is a csv file where each row has a
feature name in column A and an estimate in whole days in column B. You can
optionally specify the number of people working on the project, the project
name and the start date as command line arguments.
"""

def main():
  parser = optparse.OptionParser(usage="%prog [options] <features.csv>", description=DESCRIPTION)
  parser.add_option("-n", "--project-name", dest="project_name", default="Mystery Project", help="Use NAME as the name for the project", metavar="NAME")
  parser.add_option("-p", "--people", dest="num_people", type="int", default=4, help="Assume there are N people working on the project (default=4)", metavar="N")
  parser.add_option("-s", "--start-date", dest="start_date", default=None, help="Specify the start date for the project (default=Monday of this week)", metavar="DATE")
  opts, args = parser.parse_args()

  if len(args) != 1:
    parser.error("No feature list provided")

  if opts.num_people <= 0:
    parser.error("Can't schedule a project unless there is 1 or more people on it")

  default_start_date = datetime.date.today() - datetime.timedelta(datetime.date.today().weekday())
  if opts.start_date is None:
    # If no start date is specified 
    opts.start_date = default_start_date
  else:
    # Parse the specified start date, assuming ISO format (yyyy-mm-dd)
    opts.start_date = datetime.datetime.strptime(opts.start_date, "%Y-%m-%d").date()
    # If the start date is on a weekend, move it to the following Monday
    if opts.start_date.weekday() == 5:
      opts.start_date += datetime.timedelta(2)
    elif opts.start_date.weekday() == 6:
      opts.start_date += datetime.timedelta(1)

  # Read the feature list from the input file
  features = []
  with open(args[0], "rb") as csvfile:
    csvreader = csv.reader(csvfile)
    for row in csvreader:
      # row[0] is the feature name, row[1] is the estimate in days to develop it.
      features.append( (row[0], int(row[1])) )
  num_features = len(features)

  total_days = [ 0 ] * opts.num_people
  assignments = [ list() for _ in xrange(opts.num_people) ]

  # Assign features to people, in the order they appear in the list.
  while features != []:
    # Get the feature to assign
    feature, days = features.pop(0)

    # Decide who to assign it to. This will just be the person with the fewest
    # days of work currently assigned to them.
    assignee = 0
    for i in xrange(1, len(assignments)):
      if total_days[i] < total_days[assignee]:
        assignee = i

    # Do the assignment
    total_days[assignee] += days
    assignments[assignee].append( (feature, total_days[assignee]) )

  # Write out the assignments
  print """\
<html>
<head>
<title>%s timeline</title>
</head>
<body>
  <h1 align="center">%s timeline</h1>
  <h2 align="center">%d features, %d people, starting on %s</h2>
  <table cellpadding="8" cellspacing="0" border="1" align="center">
    <thead>
      <tr>
        <th>Date</th>""" % (opts.project_name, opts.project_name, num_features, opts.num_people, opts.start_date.strftime("%a %b %-d, %Y"))
  
  for i in xrange(opts.num_people):
    print "        <th>Person #%d</th>" % i

  print """\
      </tr>
    </thead>
    <tbody>"""

  task_end = [ 0 ] * opts.num_people
  for day_num in xrange(max(total_days)):
    d = opts.start_date + datetime.timedelta((day_num / 5) * 7 + day_num % 5)
    print "      <tr>"
    print "        <td>%s</td>" % d.strftime("%a, %d/%m")
    for i in xrange(opts.num_people):
      if day_num == task_end[i] and assignments[i] != []:
        feature, task_end[i] = assignments[i].pop(0)
        print '        <td rowspan="%d">%s</td>' % (task_end[i] - day_num, feature)
    print "      </tr>"

  print """\
    </tbody>
  </table>
</body>
</html>
"""

if __name__ == '__main__':
  main()
